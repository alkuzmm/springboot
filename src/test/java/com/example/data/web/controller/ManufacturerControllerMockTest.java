package com.example.data.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.exception.ApplicationGlobalException;
import com.example.service.ManufacturerService;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ManufacturerControllerMockTest extends ControllerBaseTest {

    @MockBean
    private ManufacturerService manufacturerService;

    @Test
    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception {
        //when
        when(manufacturerService.findAll()).thenReturn(List.of());

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {
        Manufacturer manufacturer1 = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .id(1L)
                .build();

        when(manufacturerService.findAll()).thenReturn(List.of(manufacturer1));

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Detroit")))
                .andExpect(jsonPath("$[0].companyName", is("Ford")))
                .andExpect(jsonPath("$[0].carModelName", is("Ford")))
                .andExpect(jsonPath("$[0].foundationYear", is(1903)))
                .andExpect(jsonPath("$[0].id", is(1)));
    }

    @Test
    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {

        Manufacturer manufacturer = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .id(1L)
                .build();

        doAnswer((Answer<Void>) invocation -> null).when(manufacturerService).save(manufacturer);

        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(manufacturer)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void whenListOfManufacturersListShouldRespond500Test() throws Exception {

        when(manufacturerService.findAll())
                .thenThrow(new ApplicationGlobalException());

        mockMvc.perform(get("/manufacturers"))
        .andExpect(status().is(500))
        .andDo(print());

    }














}
