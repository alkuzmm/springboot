package com.example.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Builder
@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column(name = "engine_capacity")
    private Double engineCapacity;

    @Column(name = "seats")
    private Integer seats;

    @Column(name = "wheels")
    private Integer wheels;

    @Column(name = "VIN", unique = true)
    private String vinNumber;

    @Column(name = "mass")
    private Double vehicleMass;

    @Column(name = "color")
    private String color;

    @Column(name = "model")
    private String model;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id")
    @JsonIgnore
    private Manufacturer manufacturer;
}