package com.example.data.dao;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleDAO {

    Vehicle findById(Long id); //R - read

    Vehicle save(Vehicle vehicle);

    List<Vehicle> findAll();

    void delete(Vehicle vehicle);

    void update(Vehicle vehicle);

}
