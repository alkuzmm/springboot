CREATE TABLE manufacturer
(
    id              bigserial NOT NULL,
    address         character varying(255),
    car_model_name  character varying(255),
    company_name    character varying(255),
    foundation_year integer,
    CONSTRAINT manufacturer_pkey PRIMARY KEY (id),
    CONSTRAINT uk_ritvgqej1no6x32toyp9ce0dg UNIQUE (company_name)

);